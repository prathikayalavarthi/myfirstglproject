//
//  ViewController.h
//  ScrollBarView
//
//  Created by Pratheeka on 13/09/17.
//  Copyright © 2017 Pratheeka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic,strong) UILabel* firstTextLabel;
@property (nonatomic,strong) UILabel* secondTextLabel;
@property (nonatomic,strong) UILabel* thirdTextLabel;
@property (nonatomic, strong) UIScrollView *scrollView;
@end

